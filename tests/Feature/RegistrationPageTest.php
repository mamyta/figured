<?php

namespace Tests\Feature;

use Faker\Factory;
use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegistrationPageTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRegistrationPageAvailabe()
    {
        $response = $this->get(route('register'));

        $response->assertStatus(200);
    }

    public function testRegistrationAction()
    {
        $email = Factory::create()->email;
        $user_name = 'Unit user '.random_int(1000000,9999999);
        $password = random_int(1000000,9999999);

        $user = [
            'name' => $user_name,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password
        ];

        $response = $this->post(route('register'),$user);

        $response->assertStatus(302);
        $response->assertRedirect(route('blog.home'));

        unset($user['password']);
        unset($user['password_confirmation']);
        $this->assertDatabaseHas('users', $user);
    }

}
