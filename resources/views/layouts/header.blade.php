<nav class="navbar navbar-expand-md navbar-light bg-white absolute-top">
    <div class="container">

        <button class="navbar-toggler order-2 order-md-1" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <a class="navbar-brand mx-auto order-1 order-md-2" href="{{ url('/') }}" id="log">App Store Observer</a>

        <div class="collapse navbar-collapse order-3 order-md-3" id="navbar">
            <ul class="navbar-nav mr-auto">
                @include('layouts.top_menu', ['categories' => $categories])
            </ul>
        </div>



        <div class="collapse navbar-collapse order-4 order-md-4" id="navbar">
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">Register</a>
                </li>
                @else
                    <li class="nav-item dropdown">

                        <a class="nav-link dropdown-toggle" href="#" id="dropdownAuth" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</a>
                        <div class="dropdown-menu" aria-labelledby="dropdownAuth">
                            <a class="dropdown-item" href="{{route('admin.index')}}">Dashboard</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                    @endguest
            </ul>
        </div>
    </div>
</nav>