@foreach ($categories as $category)
    @if ($category->children->count())
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown{{$category->slug}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{$category->title}} <span class="caret"></span>
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdown{{$category->slug}}">
                <a class="dropdown-item" href="{{url("/blog/category/$category->slug")}}">{{$category->title}}</a>
                <div class="dropdown-divider"></div>
                @include('layouts.top_menu', ['categories' => $category->children])
            </div>
        </li>
    @else
        @if($category->parent_id)
            <a class="dropdown-item" href="{{url("/blog/category/$category->slug")}}">{{$category->title}}</a>
        @else
            <li class="nav-item">
                <a class="nav-link" href="{{url("/blog/category/$category->slug")}}">{{$category->title}}</a>
            </li>
        @endif
    @endif
@endforeach