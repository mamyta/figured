@extends('layouts.app')

@section('content')
    @yield('jumbotron')
    <main role="main" class="container">
        <div class="row">
            <div class="col-md-8 blog-main">
                @yield('category_title')
                @yield('blog')
            </div><!-- /.blog-main -->

            <aside class="col-md-4 blog-sidebar">

                <div class="p-3 mb-3 bg-light rounded">
                    <h4 class="font-italic">About</h4>
                    <p class="mb-0">This blog is made for Figured (https://www.figured.com)</p>
                </div>

                @yield('sidebar')

                <div class="p-3">
                    <h4 class="font-italic">Links</h4>
                    <ol class="list-unstyled">
                        <li><a href="https://gitlab.com/mamyta/figured">GitLab</a></li>
                        <li><a href="https://www.linkedin.com/in/michael-vinogradov-40237618/">LinkedIn</a></li>
                    </ol>
                </div>
            </aside><!-- /.blog-sidebar -->

        </div><!-- /.row -->

    </main>
@endsection