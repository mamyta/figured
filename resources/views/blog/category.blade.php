@extends('blog.skeleton')

@section('category_title')
    <h3 class="pb-3 mb-4 font-italic border-bottom">
        Category {{$category->title}}
    </h3>
@endsection
@section('jumbotron')
    <div class="container">
        <img src="https://source.unsplash.com/1110x250/?{{str_replace(" ",",",$category->title)}}" class="img-fluid" alt="{{$category->title}}">
    <hr>
    </div>
@endsection

@section('blog')
    @forelse($posts as $post)
        <div class="blog-post">
            <h2 class="blog-post-title"><a href="{{route('post',$post->slug)}}">{{$post->title}}</a></h2>
            <p class="blog-post-meta">{{ date('j F Y', strtotime($post->created_at)) }} </p>
            @if($post->image)
                <img src="{{$post->image}}" class="img-fluid" alt="{{$post->title}}">
            @endif
            {!! $post->description_short !!}
            <hr>
        </div><!-- /.blog-post -->
    @empty
        <div class="blog-post">
            <h2 class="blog-post-title">Empty</h2>
        </div>
    @endforelse
@endsection