@extends('blog.skeleton')

@section('category_title')
@endsection
@section('blog')
    <div class="blog-post">
        {{debug($post)}}
        <h1 class="blog-post-title">{{$post->title}}</h1>
        <p class="blog-post-meta">{{ date('j F Y', strtotime($post->created_at)) }} , {{ $post->view_count }} views</p>
    @if($post->image)
            <img src="{{$post->image}}" class="img-fluid" alt="{{$post->title}}">
        @endif
        {!! $post->description !!}
        <hr>
    </div><!-- /.blog-post -->
@endsection