@extends('blog.skeleton')

@section('category_title')
    <h3 class="pb-3 mb-4 font-italic border-bottom">
        Last 10 news
    </h3>
@endsection

@section('jumbotron')
@if(isset($pinned))
    <div class="container">
        <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark" @if($pinned->image) style="background-image: url({{$pinned->image}}); background-size: 100% 100%;" @endif>
            <div class="col-md-6 px-0">
                <h1 class="display-4 font-italic">{{$pinned->title}}</h1>
                <p class="lead my-3">{!! $pinned->description_short !!}</p>
                <p class="lead mb-0"><a href="{{route('post',$pinned->slug)}}" class="text-white font-weight-bold">Continue reading...</a></p>
            </div>
        </div>
    </div>
@endif
@endsection

@section('blog')
    @forelse($posts as $post)
        <div class="blog-post">
            <h2 class="blog-post-title"><a href="{{route('post',$post->slug)}}">{{$post->title}}</a></h2>
            <p class="blog-post-meta">{{ date('j F Y', strtotime($post->created_at)) }}, Category: <a href="{{route('category',$post->category->slug)}}">{{$post->category->title}}</a></p>
            @if($post->image)
                <img src="{{$post->image}}" class="img-fluid" alt="{{$post->title}}">
            @endif
            {!! $post->description_short !!}
            <hr>
        </div><!-- /.blog-post -->
    @empty
        <div class="blog-post">
            <h2 class="blog-post-title">Empty</h2>
        </div>
    @endforelse
@endsection
