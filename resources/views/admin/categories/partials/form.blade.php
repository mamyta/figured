<label for="">Title</label>
<input type="text" class="form-control" name="title" placeholder="Category title" value="{{$category->title or ""}}" required>

<label for="">Slug</label>
<input class="form-control" type="text" name="slug" placeholder="Generate" value="{{$category->slug or ""}}" readonly="">

<label for="">Parent category</label>
<select class="form-control" name="parent_id">
    <option value="0">-- without parent category --</option>
    @include('admin.categories.partials.categories', ['categories' => $categories])
</select>

<label for="">Description</label>
<textarea class="form-control" name="description">
{{$category->description or ""}}
</textarea>
<hr />

<input class="btn btn-primary" type="submit" value="Save">