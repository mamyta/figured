@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">

        @component('admin.components.breadcrumb')
        @slot('title') Edit Post @endslot
        @slot('parent') Home @endslot
        @slot('active') Posts @endslot
        @endcomponent

        <hr />

        <form class="form-horizontal" action="{{route('admin.post.update',$post)}}" method="post">
            <input type="hidden" name="_method" value="put">
            {{ csrf_field() }}

            {{-- Form include --}}
            @include('admin.posts.partials.form')

        </form>
    </div>

@endsection