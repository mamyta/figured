<label for="">Title</label>
<input type="text" class="form-control" name="title" placeholder="Post title" value="{{$post->title or ""}}" required>

<label for="">Status</label>
<select class="form-control" name="published">
    @if (isset($post->id))
        <option value="0" @if ($post->published == 0) selected="" @endif>Draft</option>
        <option value="1" @if ($post->published == 1) selected="" @endif>Published</option>
    @else
        <option value="0">Draft</option>
        <option value="1">Published</option>
    @endif
</select>

<div class="form-check">
        <input class="form-check-input" name=pinned type="checkbox" value="1" id="defaultCheck1"  @if (isset($post->id)&& ($post->pinned == 1)) checked @endif>
        <label class="form-check-label" for="defaultCheck1">
                Pinned on Home Page
        </label>
</div>

<label for="">Slug (uniq)</label>
<input class="form-control" type="text" name="slug" placeholder="Generate" value="{{$post->slug or ""}}" readonly="">

<label for="">Category</label>
<select class="form-control" name="category_id">
    @include('admin.posts.partials.categories', ['categories' => $categories,'post'=>$post])
</select>

<label for="">Image URL</label>
<input type="text" class="form-control" name="image" placeholder="Image url" value="{{$post->image or ""}}" required>


<label for="">Short description</label>
<textarea class="form-control" id="description_short" name="description_short">{{$post->description_short or ""}}</textarea>


<label for="">Description</label>
<textarea class="form-control" id="description" name="description">{{$post->description or ""}}</textarea>

<hr />

<input class="btn btn-primary" type="submit" value="Save">