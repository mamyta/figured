@foreach ($categories as $category_list)
    <option value="{{$category_list->id or ""}}"
            @isset($post->category_id)
                @if ($post->category_id == (string)$category_list->id)
                selected
                @endif
            @endisset
    >
        {!! $delimiter or "" !!}{{$category_list->title or ""}}
    </option>

    @if (count($category_list->children) > 0)

        @include('admin.posts.partials.categories', [
          'categories' => $category_list->children,
          'delimiter'  => ' - ' . $delimiter,
          'post' => $post
        ])

    @endif
@endforeach