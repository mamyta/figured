@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumb')
        @slot('title') Posts list @endslot
        @slot('parent') Home @endslot
        @slot('active') Posts @endslot
        @endcomponent
        <table class="table table-striped">
            <thead>
            <th>Title</th>
            <th>Category</th>
            <th>Status</th>
            <th class="text-right"><a href="{{route('admin.post.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"></i> Add Post</a></th>
            </thead>
            <tbody>
            @forelse ($posts as $post)
                <tr>
                    <td>{{$post->title}}</td>
                    <td>{{$post->category->title or ""}}</td>
                    <td>@if ($post->published== 1 ) Published @else Draft @endif</td>
                    <td class="text-right">
                        <form onsubmit="if(confirm('Delete?')){return true}else{return false}" action="{{route('admin.post.destroy',$post)}}" method="post">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field() }}
                            <a class="btn btn-link" href="{{route('admin.post.edit', $post)}}"><i class="fa fa-edit"></i></a>
                            <button type="submit" class="btn btn-link"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center"><h2>No data</h2></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>

@endsection