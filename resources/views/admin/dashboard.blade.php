@extends('admin.layouts.app_admin')

@section('content')
   <div class="container">
       <div class="row">
           <div class="col-sm-3">
               <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
                   <div class="card-body">
                       <h5 class="card-title">Categories {{$count_categories or "0"}}</h5>
                   </div>
               </div>
           </div>
           <div class="col-sm-3">
               <div class="card text-white bg-primary mb-3" style="max-width: 18rem;">
                   <div class="card-body">
                       <h5 class="card-title">Posts {{$count_posts or "0"}}</h5>
                   </div>
               </div>
           </div>
           <div class="col-sm-3">
               <div class="card text-white bg-warning mb-3" style="max-width: 18rem;">
                   <div class="card-body">
                       <h5 class="card-title">Views {{$count_views or "0"}}</h5>
                   </div>
               </div>
           </div>
           <div class="col-sm-3">
               <div class="card text-white bg-danger mb-3" style="max-width: 18rem;">
                   <div class="card-body">
                       <h5 class="card-title">Users {{$count_users or "0"}}</h5>
                   </div>
               </div>
           </div>
       </div>
       <div class="row">
           <div class="col-sm-6">
               <a class="btn btn-block btn-default" href="{{route('admin.category.create')}}">New Category</a>
               <ul class="list-group">
                   @foreach($categories as $category)
                   <li class="list-group-item d-flex justify-content-between align-items-center">
                       <a href="{{route('admin.category.edit', $category)}}">{{$category->title}}</a>
                       <span class="badge badge-primary badge-pill">{{$category->posts()->count()}}</span>
                   </li>
                   @endforeach
               </ul>

           </div>
           <div class="col-sm-6">
               <a class="btn btn-block btn-default" href="{{route('admin.post.create')}}">New Post</a>
               <div class="list-group">
                   @foreach($posts as $post)
                   <a href="{{route('admin.post.edit',$post)}}" class="list-group-item list-group-item-action flex-column align-items-start">
                       <div class="d-flex w-100 justify-content-between">
                           <h5 class="mb-1">{{$post->title}}</h5>
                           <small class="text-muted">@if ($post->published== 1 ) Published @else Draft @endif</small>
                       </div>
                       <p class="mb-1">{!! $post->description_short !!}</p>
                       <small>{{$post->category->title or ""}}</small>
                   </a>
                   @endforeach
               </div>
           </div>
       </div>
   </div>
@endsection