<?php
/**
 * Created by PhpStorm.
 * User: mamyta
 * Date: 20/05/2018
 * Time: 2:09
 */

namespace App;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Str;

class Post extends Eloquent
{
    protected $connection = 'mongodb';

    // mass assigned
    protected $fillable = array(
        'title',
        'slug',
        'category_id',
        'description_short',
        'description',
        'image',
        'pinned',
        'published'
    );

    public function setSlugAttribute($value){
        $this->attributes['slug'] = Str::slug( mb_substr($this->title,0 ,40)."-". \Carbon\Carbon::now()->format('dmyHi'),'-');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function scopeLastPosts($query,$count,$skip=0){
        $posts = Post::where('published','=','1')->orderBy('created_at', 'desc')->skip($skip)->take($count)->get();
        return $posts;
    }

    public function scopeGetPinned($query){
        $post = Post::where('pinned','=','1')->first();
        return $post;
    }

    public function scopeTotalViews($query){
        return Post::sum('view_count');
    }
}