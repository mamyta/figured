<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;
use Session;

class BlogController extends Controller
{
    public function category($slug)
    {
        $category = Category::where('slug', $slug)->first();

        return view('blog.category', [
            'category' => $category,
            'posts' => $category->posts()
        ]);
    }

    public function post($slug)
    {
        $post = Post::where('slug', '=', $slug)->first();

        $viewCountKey = 'blog_' . $post->id;
        if (!Session::has($viewCountKey)) {

            $post->view_count += 1;
            $post->save();

            Session::put($viewCountKey, 1);
        }

        return view('blog.post', [
            'post' => $post
        ]);
    }

    public function home()
    {
        return view('blog.home', [
            'pinned' => Post::getPinned(),
            'posts' => Post::lastPosts(10)
        ]);
    }
}
