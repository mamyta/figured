<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Category;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //Dashboard
    public function dashboard(){
        return view('admin.dashboard',[
            'categories' => Category::lastCategories(5),
            'posts' => Post::lastPosts(3),
            'count_categories' => Category::count(),
            'count_posts' => Post::count(),
            'count_users' => User::count(),
            'count_views' => Post::sum('view_count')
        ]);
    }
}
