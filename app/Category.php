<?php

namespace App;
use Eloquent;
use App\Post;

use Jenssegers\Mongodb\Eloquent\HybridRelations;

use Illuminate\Support\Str;

class Category extends Eloquent
{
    use HybridRelations;

    // mass assigned
    protected $fillable = array(
        'title',
        'slug',
        'parent_id',
        'description'
    );

    protected $connection = 'mysql';

    public function setSlugAttribute($value){
        $this->attributes['slug'] = Str::slug( mb_substr($this->title,0 ,40)."-". \Carbon\Carbon::now()->format('dmyHi'),'-');
    }

    public function posts()
    {
        return Post::where('category_id','=', (string)$this->id)->where('published','=','1')->get();
    }

    public function children(){
        return $this->hasMany(self::class,'parent_id');
    }

    public function scopeLastCategories($query,$count){
        return $query->orderBy('created_at','desc')->take($count)->get();
    }

}
